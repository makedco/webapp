# webapp

The [makedco](https://makedco.io) single page application. Built using Typescript, React, Mobx and web3; deployed with Zeit.
