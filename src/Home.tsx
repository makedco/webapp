import React from 'react'
import { observer, inject } from 'mobx-react'
import MakeStore, { homesteadSource, rinkebySource, ropstenSource } from './stores/make'
import AuthStore from './stores/auth'
import { GitlabLogo } from './components/Icons'
import AuctionCell from './components/AuctionCell'
import ContractInfo from './components/ContractInfo'
import Graph from './components/Graph'
import { white, gray, yellow, green } from './Colors'
import AuctionTableHeader from './components/AuctionTableHeader'

@inject('auth', 'make')
@observer
export default class Home extends React.Component<{
  make: MakeStore
  auth: AuthStore
}> {
  state = {
    purchaseInput: '',
  }

  async componentDidMount() {
    await this.props.make.loadCurrentWeiPrice()
  }

  sigmoid = (x: number, width: number, height: number) => {
    const yMax = height - 40
    const yMin = 20

    const xMax = width
    const yRange = yMax - yMin

    // Controls sigmoid steepness
    const k = 2000

    const b = 4 * Math.sqrt(k + x * x + xMax * xMax / 4 - x * xMax)
    const s = yRange / 2 + 2 * x * yRange / b - xMax * yRange / b
    return yMin + s
  }

  verticalLineX = (width: number) => {
    const { make } = this.props
    return make.cycleSecondsRemaining() * width / +make.cycleLengthSeconds.toString()
  }

  horizontalLineY = (height: number) => {
    const { make } = this.props
    const etherRatio = make.realEtherPrice()
    const etherRatioMax = 0.001
    const etherRatioMin = 0.0001
    if (+etherRatio < etherRatioMin) return -500
    const yMax = height - 40
    const yMin = 20
    const yRange = yMax - yMin
    const x = +etherRatio - etherRatioMin
    const xRange = etherRatioMax - etherRatioMin
    return yMin + yRange - x * yRange / xRange
  }

  onNetworkSelectorChange = (e: any) => {
    const { make } = this.props
    if (e.target.value === 'homestead') {
      make.setContractSource(homesteadSource)
    } else if (e.target.value === 'rinkeby') {
      make.setContractSource(rinkebySource)
    } else if (e.target.value === 'ropsten') {
      make.setContractSource(ropstenSource)
    }
  }

  render() {
    const { make, auth } = this.props
    const ethPrice = make.web3.utils.fromWei(make.weiPrice.toString()).toString()
    const roundedEthPrice = parseFloat(ethPrice).toFixed(DECIMAL_PLACES + 2)
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontFamily: 'helvetica',
        backgroundColor: white,
        color: gray,
        flex: 1,
      }}>
        <a
          href="https://gitlab.com/makedco/contracts/blob/master/README.md#makedco"
          target="_blank"
          style={{
            color: gray,
            textDecoration: 'none',
          }}
        >
          <div
            style={{
              fontSize: 19,
              fontWeight: 'bold',
              padding: 2,
              border: `2px solid ${gray}`,
            }}
          >
            makeDCO
          </div>
        </a>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            textAlign: 'center',
            backgroundColor: gray,
            padding: 8,
            margin: 8,
          }}
        >
          <ContractInfo />
          <Graph
            s={this.sigmoid}
            verticalLineX={this.verticalLineX}
            horizontalLineY={this.horizontalLineY}
            currentPriceString={roundedEthPrice}
            realPriceString={make.realEtherPrice().toString()}
          />
          {auth.canMakeTransaction ? (
            <p style={{ color: white }}>
              Your mDCO balance: <span style={{ color: green }}>{make.balances[auth.activeAddress] || '0'}</span>
            </p>
          ) : null}
          {make.networkId > 1 ? (
            <p style={{ color: yellow, padding: 8, backgroundColor: gray, borderRadius: 4 }}>
              This is pre-release software on a testnet
            </p>
          ): null}
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around',
            width: '75%',
            minWidth: 400,
            padding: 4,
            marginBottom: 8,
          }}
        >
          <AuctionTableHeader />
          {Array.apply(null, Array(make.auctionCount)).map((_: any, index: number) => (
            <AuctionCell key={index} auctionIndex={index} />
          ))}
        </div>
        <a
          href={`https://${make.etherscanPrefix()}etherscan.io/token/${make.contractAddress}#balances`}
          target="_blank"
          style={{
            marginBottom: 8,
            color: gray,
          }}
        >
          {make.contractAddress}
        </a>
        <span dangerouslySetInnerHTML={{
          __html: `<!--
          "mDCO is not a security; it's used in DCO's"
          - me getting arrested by the SEC probably
        -->`}} />
        <select
          onChange={this.onNetworkSelectorChange}
          value={
            make.networkId === 3 && 'ropsten' ||
            make.networkId === 4 && 'rinkeby' ||
            'homestead'
          }
        >
          <option value="homestead">Homestead</option>
          <option value="rinkeby">Rinkeby</option>
          <option value="ropsten">Ropsten</option>
        </select>
        <a href="https://gitlab.com/makedco/contracts#README.md" target="_blank">
          <GitlabLogo />
        </a>
      </div>
    )
  }
}
