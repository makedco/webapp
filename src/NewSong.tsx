import React from 'react'

export default () => (
  <div>
    <iframe
      width="100%"
      height="300"
      scrolling="no"
      frameBorder="no"
      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/657808568%3Fsecret_token%3Ds-Fs8yr&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
    />
  </div>
)
