import React from 'react'
import { observer, inject } from 'mobx-react'
import MakeStore from '../stores/make'
import hashicon from 'hashicon'
import { EthereumLogo, CoinsLogo, RightArrow } from './Icons'
import Input from './Input'
import Button from './Button'
import { white, gray, yellow } from '../Colors'
import AuthStore from '../stores/auth'
import idx from 'idx'
import BN from 'bn.js'
import CreateAuctionCell from './CreateAuctionCell'
import Popup from './Popup'

@inject('make', 'auth')
@observer
export default class AuctionCell extends React.Component<{
  make?: MakeStore
  auth?: AuthStore
  auctionIndex: number
}> {
  timer: any

  state = {
    buyTokenValue: '',
    newAuctionUrl: '',
    newAuctionName: '',
    showingAuctionPurchase: false,
  }

  async componentDidMount() {
    const { make, auctionIndex } = this.props
    if (auctionIndex < make.auctionCount) {
      make.loadAuctionIndex(auctionIndex)
    }
    this.timer = setInterval(() => {
      if (auctionIndex >= make.auctionCount) return
      // This reloads the auction, and tokens available for purchase
      make.loadAuctionIndex(auctionIndex)
    }, 15000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  purchaseTokensFromAuction = async () => {
    if (!window.ethereum) {
      return alert('Install MetaMask to use this dapp')
    }
    const { auth, make, auctionIndex } = this.props
    const { buyTokenValue } = this.state
    if (isNaN(+buyTokenValue)) {
      return alert('Invalid mDCO value specified')
    } else if (+buyTokenValue <= 0) {
      return alert('mDCO amount must be greater than 0')
    }
    await make.loadCurrentWeiPrice()
    const totalWei = +buyTokenValue * make.weiPrice
    await auth.send({
      to: make.contractAddress,
      data: make.contract.methods.buyTokens(+auctionIndex, +buyTokenValue).encodeABI(),
      value: make.web3.utils.toHex(totalWei),
    })
    this.setState({ sellEtherValue: '' })
  }

  saveNameAndUrl = async () => {
    if (!window.ethereum) {
      return alert('Install MetaMask to use this dapp')
    }
    const { auth, make, auctionIndex } = this.props
    const { newAuctionUrl, newAuctionName } = this.state
    if (newAuctionName) {
      await auth.send({
        to: make.contractAddress,
        data: make.contract.methods.setName(+auctionIndex, newAuctionName).encodeABI(),
      })
      this.setState({ newAuctionName: '' })
    }
    make.loadAuctionIndex(auctionIndex)
    if (newAuctionUrl) {
      await auth.send({
        to: make.contractAddress,
        data: make.contract.methods.setUrl(+auctionIndex, newAuctionUrl).encodeABI(),
      })
      this.setState({ newAuctionUrl: '' })
    }
    await make.loadAuctionIndex(auctionIndex)
  }

  render() {
    const { make, auth, auctionIndex } = this.props
    const auction = make.auctions[auctionIndex]
    if (!auction) return (
      <div style={{ margin: 4 }}>Loading...</div>
    )
    const cyclesRemaining = +auction.endCycle.toString() - +make.currentCycle
    if (cyclesRemaining < 0) return null
    const tokensAvailable = +(make.tokensAvailablePerAuction[auctionIndex] || 1000)
    return (
      <>
        <Popup
          onClick={() => {
            this.setState({
              showingAuctionPurchase: false,
            })
          }}
          visible={this.state.showingAuctionPurchase}
        >
          <CreateAuctionCell
            onCreated={() => this.setState({
              showingAuctionPurchase: false
            })}
          />
        </Popup>
        <div style={{
          margin: 4,
          padding: 8,
          boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.2)',
          backgroundColor: gray,
          color: white,
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          flexWrap: 'wrap',
          flex: 1,
        }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div style={{ fontWeight: 'bold' }}>
              {+auction.startCycle.toString() > +make.currentCycle ? (
                <div style={{ marginBottom: 8, color: yellow, fontSize: 16, fontWeight: 'bold' }}>
                  Starts next cycle
                </div>
              ) : null}
              <a style={{ color: white }} href={auction.url || 'https://makedco.io'} target="_blank">
                {auction.name || 'Unnamed Auction'}
              </a>
            </div>
            <div style={{ height: 10 }} />
            <div style={{ }}>
              Cycles Remaining: {
                cyclesRemaining < 10**10 ?
                cyclesRemaining :
                parseFloat(cyclesRemaining.toString()).toExponential(DECIMAL_PLACES)
              }
            </div>
            <div style={{ height: 10 }} />
          </div>
          <div style={{ height: 1, flex: 1 }} />
            {auction.creator.toLowerCase() === idx(window, (_) => _.ethereum.selectedAddress.toLowerCase()) ? (
              <div style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 2,
                margin: 8,
              }}>
                <Input
                  placeholder={auction.name || 'Auction Name'}
                  onKeyDown={(e: any) => {
                    if (e.key !== 'Enter') return
                    this.saveNameAndUrl()
                  }}
                  onChange={(e: any) => {
                    this.setState({
                      newAuctionName: e.target.value,
                    })
                  }}
                  value={this.state.newAuctionName}
                />
                <Input
                  placeholder={auction.url || 'Auction URL'}
                  onKeyDown={(e: any) => {
                    if (e.key !== 'Enter') return
                    this.saveNameAndUrl()
                  }}
                  onChange={(e: any) => {
                    this.setState({
                      newAuctionUrl: e.target.value,
                    })
                  }}
                  value={this.state.newAuctionUrl}
                />
                <Button onClick={this.saveNameAndUrl}>Save</Button>
              </div>
            ) : null}
            {auctionIndex === 0 ? (
              <div style={{ margin: 8 }}>
                <Button
                  onClick={() => {
                    this.setState({
                      showingAuctionPurchase: true,
                    })
                  }}
                >
                  Purchase Auction
                </Button>
              </div>
            ) : null}
            {auctionIndex === 0 ? null : (
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 2,
                  margin: 8,
                }}
              >
                <div style={{ minWidth: 80 }}>
                  <Input
                    placeholder={`max ${tokensAvailable} mDCO`}
                    onKeyDown={(e: any) => {
                      if (e.key === 'Enter') {
                        this.purchaseTokensFromAuction()
                      }
                    }}
                    onChange={(e: any) => {
                      if (isNaN(+e.target.value) && e.target.value !== '.') return
                      this.setState({
                        buyTokenValue: e.target.value
                      })
                    }}
                    value={this.state.buyTokenValue}
                  />
                </div>
                <div style={{ margin: 4 }}>
                  for ~ {parseFloat(+this.state.buyTokenValue * +make.web3.utils.fromWei(make.weiPrice.toString())).toFixed(DECIMAL_PLACES)} Eth
                </div>
                <Button onClick={this.purchaseTokensFromAuction}>
                  Buy
                </Button>
              </div>
            )}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              justifyContent: 'space-around',
              margin: 8,
            }}
          >
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <CoinsLogo style={{ marginRight: 8, width: 30, height: 30, fill: white }} />
              <div>
                {tokensAvailable.toString()}
              </div>
            </div>
            <div style={{ height: 8 }} />
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <EthereumLogo style={{ marginLeft: 8, width: 30, height: 30, fill: white }} />
              <div>
                {parseFloat(make.web3.utils.fromWei(auction.totalWeiIngested.toString())).toFixed(DECIMAL_PLACES)}
              </div>
            </div>
            <div style={{ width: 10 }} />
          </div>
          <div style={{
            width: 10,
          }} />
          <a
            style={{ textDecoration: 'none' }}
            href={`https://${make.etherscanPrefix()}etherscan.io/address/${auction.creator}`}
            target="_blank"
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 8,
                margin: 4
              }}
            >
              <img
                src={hashicon(auction.creator, 200).toDataURL()}
                width={50}
                height={50}
              />
              <div style={{ marginTop: 4, fontSize: 12, color: white }}>
                {auction.creator.slice(0, 9)}
              </div>
            </div>
          </a>
        </div>
      </>
    )
  }
}
