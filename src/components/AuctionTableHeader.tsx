import React from 'react'
import Input from './Input'
import Button from './Button'
import { inject, observer } from 'mobx-react'
import MakeStore from '../stores/make'
import AuthStore from '../stores/auth'

@inject('make', 'auth')
@observer
export default class AuctionTableHeader extends React.Component<{
  make?: MakeStore
  auth?: AuthStore
}> {
  state = {
    purchaseInput: '',
  }

  purchaseTokens = async () => {
    if (!window.ethereum) {
      return alert('Install MetaMask to use this dapp')
    }
    const { auth, make } = this.props
    const { purchaseInput } = this.state
    await auth.send({
      to: make.contractAddress,
      data: make.contract.methods.batchBuyTokens().encodeABI(),
      value: make.web3.utils.toHex(make.web3.utils.toWei(purchaseInput).toString()),
    })
    this.setState({
      purchaseInput: '',
    })
  }

  render() {
    const { make } = this.props
    const ethPrice = make.web3.utils.fromWei(make.weiPrice.toString()).toString()
    const maxEthAvailable = make.availableTokens() * +make.web3.utils.fromWei(make.weiPrice.toString())
    return (
      <div style={{ margin: 8, display: 'flex', justifyContent: 'space-between' }}>
        <div style={{ display: 'flex', flexDirection: 'column', fontSize: 19, fontWeight: 'bold' }}>
          <div style={{ flex: 1 }} />
          Auctions
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div>
            <Input
              placeholder={`max ${parseFloat(maxEthAvailable.toString()).toFixed(DECIMAL_PLACES)} Eth`}
              onKeyDown={(e: any) => {
                if (e.key === 'Enter') {
                  this.purchaseTokens()
                }
              }}
              onChange={(e: any) => {
                if (isNaN(+e.target.value) && e.target.value !== '.') return
                if (e.target.value > 10000) return
                this.setState({
                  purchaseInput: e.target.value,
                })
              }}
              value={this.state.purchaseInput}
            />
            <div style={{ textAlign: 'right', marginRight: 4 }}>
              for ~ {Math.floor(+this.state.purchaseInput / +ethPrice)} mDCO
            </div>
          </div>
          <Button onClick={this.purchaseTokens}>
            Sell
          </Button>
        </div>
      </div>
    )
  }
}
