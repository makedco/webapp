import React from 'react'
import { gray, white } from '../Colors'

export default class Button extends React.Component<{
  onClick?: () => void
  invertColors?: boolean
}> {
  state = {
    mouseOver: false,
  }
  render() {
    const { mouseOver } = this.state
    const { invertColors } = this.props
    let defaultColorScheme = mouseOver
    if (invertColors) {
      defaultColorScheme = !defaultColorScheme
    }
    return (
      <div
        onMouseOver={() => this.setState({ mouseOver: true })}
        onMouseOut={() => this.setState({ mouseOver: false })}
        style={{
          borderRadius: 0,
          padding: 4,
          margin: 4,
          cursor: 'pointer',
          border: `1px solid ${defaultColorScheme ? gray : white}`,
          backgroundColor: defaultColorScheme ? white : gray,
          color: defaultColorScheme ? gray : white,
          textAlign: 'center',
          fontSize: 14,
        }}
        onClick={() => this.props.onClick && this.props.onClick()}
      >
        {this.props.children}
      </div>
    )
  }
}
