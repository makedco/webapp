import React from 'react'
import { observer, inject } from 'mobx-react'
import MakeStore from '../stores/make'
import { yellow, lavendar, white } from '../Colors'

@inject('make')
@observer
export default class ContractInfo extends React.Component<{
  make?: MakeStore
}> {
  timer: any
  componentDidMount() {
    this.timer = setInterval(() => this.forceUpdate(), 100)
  }
  componentWillUnmount() {
    clearInterval(this.timer)
  }
  fullTimeString() {
    const creationTimestamp = +this.props.make.creationTimestamp.toString()
    const cycleLengthSeconds = +this.props.make.cycleLengthSeconds.toString()
    const timestamp = +new Date() / 1000
    const diffSeconds = timestamp - creationTimestamp
    const cycles = Math.floor(diffSeconds / cycleLengthSeconds)
    const currentCycle = cycles % (24 * 60 * 60 / +this.props.make.cycleLengthSeconds)
    const days = Math.floor(diffSeconds / (24 * 60 * 60))
    const remainingSeconds = cycleLengthSeconds - (timestamp - (creationTimestamp + cycles * cycleLengthSeconds))
    return `Day ${days}; Cycle ${currentCycle}; ${this.timeString(remainingSeconds)}`
  }

  timeString(remainingSeconds: number) {
    const remainingHours = Math.floor(remainingSeconds / (60 * 60))
    const remainingMinutes = Math.floor(remainingSeconds / 60) - remainingHours * 60
    const _remainingSeconds = Math.floor(remainingSeconds - (remainingHours * 60 * 60) - (remainingMinutes * 60))
    return `${remainingHours < 10 ? 0 : ''}${remainingHours}:${remainingMinutes < 10 ? 0 : ''}${remainingMinutes}:${_remainingSeconds < 10 ? 0 : ''}${_remainingSeconds}`
  }

  render() {
    const { make } = this.props
    return (
      <div style={{ margin: 8 }}>
        <div style={{ color: white }}>
          mDCO Total Supply: <span style={{ color: yellow }}>{make.totalSupply.toString()}</span>
        </div>
        <div style={{ margin: 4, color: lavendar }}>
          {this.fullTimeString()}
        </div>
      </div>
    )
  }
}
