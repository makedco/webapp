import React from 'react'
import { observer, inject } from 'mobx-react'
import MakeStore from '../stores/make'
import AuthStore from '../stores/auth'
import Input from './Input'
import Button from './Button'
import { white, red } from '../Colors'
import idx from 'idx'

@inject('auth', 'make')
@observer
export default class CreateAuctionCell extends React.Component<{
  make?: MakeStore
  auth?: AuthStore
  onCreated?: () => void
}> {
  state = {
    auctionName: '',
    auctionNameRequiredShowing: false,
    auctionUrl: '',
  }

  createAuction = async () => {
    if (!window.ethereum) {
      return alert('Install MetaMask to use this dapp')
    }
    const { auth, make } = this.props
    const { auctionName, auctionUrl } = this.state
    if (!auctionName) {
      this.setState({
        auctionNameRequiredShowing: true,
      })
      return
    }
    await auth.send({
      to: make.contractAddress,
      data: make.contract.methods.createAuction(auctionName, auctionUrl).encodeABI(),
      value: make.web3.utils.toHex(`${10**17}`),
    })
    this.setState({
      auctionName: '',
      auctionUrl: '',
      auctionNameRequiredShowing: false,
    }, () => idx(this.props, (_) => _.onCreated()))
  }

  render() {
    const { make } = this.props
    const { auctionNameRequiredShowing } = this.state
    const tokenCount = Math.floor(0.1 / +make.web3.utils.fromWei(this.props.make.weiPrice.toString()))
    const remainingTokens = make.tokensAvailablePerAuction[0]
    return (
      <div
        style={{
          boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
          padding: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          maxWidth: 500,
          backgroundColor: white,
        }}
      >
        <div style={{ fontWeight: 'bold' }}>
          Purchase Auction Time
        </div>
        <div style={{ padding: 4, margin: 4 }}>
          You are paying 0.1 Ether for ~{tokenCount} mDCO and the ability to mint 1000 mDCO at a pre-specified price curve (above) every 6 hours for 120 cycles (30 days). Proceeds from token sales are sent directly to the address used for this transaction.
          <br />
          <br />
          There is no guarantee that this token will have value, or that any will be bought. The guarantee is the ability to mint token and receive Ether; an experiment in basic income.
          <br />
          <br />
          Your auction will be listed below and linked to the supplied url. You can change it after.
        </div>
        {+remainingTokens > tokenCount ? (
          <>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <Input
                style={auctionNameRequiredShowing ? {
                  border: `1px solid ${red}`,
                } : {}}
                placeholder="Auction Name"
                onKeyDown={(e: any) => {
                  if (e.key === 'Enter') {
                    this.createAuction()
                  }
                }}
                onChange={(e: any) => {
                  this.setState({
                    auctionName: e.target.value,
                  })
                }}
                value={this.state.auctionName}
              />
              <Input
                placeholder="Auction URL"
                onChange={(e: any) => {
                  this.setState({
                    auctionUrl: e.target.value,
                  })
                }}
                onKeyDown={(e: any) => {
                  if (e.key === 'Enter') {
                    this.createAuction()
                  }
                }}
                value={this.state.auctionUrl}
              />
            </div>
            <Button onClick={this.createAuction}>
              Create (convert 0.1 eth to ~ {tokenCount} mDCO)
            </Button>
          </>
        ) : (
          <Button>
            More auctions available next cycle
          </Button>
        )}
      </div>
    )
  }
}
