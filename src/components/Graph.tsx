import React from 'react'
import { green, lavendar, gray, white } from '../Colors'
import Popup from './Popup'

export default class Graph extends React.Component<{
  s: (x: number, width: number, height: number) => number
  verticalLineX?: (width: number) => number
  horizontalLineY?: (height: number) => number
  currentPriceString?: string
  realPriceString?: string
}> {
  canvasRef: React.RefObject<HTMLCanvasElement> = React.createRef()
  timer: any
  state = {
    width: 300 * 2,
    height: 300 * 2,
    showDetails: false,
    showInfoPopup: false,
  }

  componentDidMount() {
    this.timer = setInterval(() => this.draw(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  draw = () => {
    if (!this.canvasRef.current) return
    const { width, height, showDetails, showInfoPopup } = this.state
    const ctx = this.canvasRef.current.getContext('2d')
    ctx.imageSmoothingEnabled = false
    ctx.clearRect(0, 0, width, height)
    ctx.beginPath()
    let yMax = 0
    let yMin = Infinity
    for (let x = 0; x < width; x++) {
      const y = this.props.s(x, width, height)
      if (y > yMax) yMax = y
      if (y < yMin) yMin = y
      // Start the path at first point
      if (x == 0) ctx.moveTo(x, y)
      // Move to next point (or current point if it's the first point)
      ctx.lineTo(x, y)
      ctx.moveTo(x, y)
    }
    ctx.lineWidth = 2
    ctx.font = '23px Helvetica'
    ctx.strokeStyle = white
    ctx.stroke()
    // Draw a horizontal line (indicating real price)
    if (this.props.horizontalLineY) {
      const pointerPositionY = this.props.horizontalLineY(height)
      ctx.beginPath()
      ctx.moveTo(0, pointerPositionY)
      ctx.lineTo(width, pointerPositionY)
      ctx.strokeStyle = green
      ctx.stroke()
      ctx.fillStyle = white
      const x = pointerPositionY > height / 2 ? 0 : width
      if (showDetails || showInfoPopup) {
        ctx.fillText('Real Price', Math.min(x, width - 110), pointerPositionY - 4)
      } else {
        ctx.fillText(this.props.realPriceString, Math.min(x, width - 70), pointerPositionY - 4)
      }
    }
    // Draw the vertical line (indicating time)
    if (this.props.verticalLineX) {
      const pointerPositionX = this.props.verticalLineX(width)
      ctx.beginPath()
      ctx.moveTo(pointerPositionX, 0)
      ctx.lineTo(pointerPositionX, height)
      ctx.strokeStyle = lavendar
      ctx.stroke()
    }
    if (this.props.verticalLineX && this.props.currentPriceString) {
      const x = this.props.verticalLineX(width) + 4
      const y = this.props.s(x, width, height)
      ctx.fillStyle = white
      if (showDetails || showInfoPopup) {
        ctx.fillText('Current Price', Math.min(x + 4, width - 140), y - 4)
      } else {
        ctx.fillText(this.props.currentPriceString, Math.min(x + 4, width - 100), y - 4)
      }
    }
    // Draw min/max markers
    ctx.fillStyle = white
    const textHeight = 20
    if (showDetails || showInfoPopup) {
      ctx.fillText('Max Price', 0, yMin + textHeight)
      ctx.fillText('Min Price', width - 100, yMax + textHeight)
    } else {
      ctx.fillText('0.001', 0, yMin + textHeight)
      ctx.fillText('0.0001', width - 70, yMax + textHeight)
    }
  }

  render() {
    return (
      <>
        <Popup visible={this.state.showInfoPopup}>
          <div
            style={{
              position: 'fixed',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0, 0, 0, 0.2)',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              cursor: 'pointer',
            }}
            onClick={() => this.setState({ showInfoPopup: false })}
          >
            <div
              style={{
                boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                padding: 8,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                maxWidth: 500,
                backgroundColor: white,
              }}
            >
              <div style={{
                fontSize: '20px',
                width: '100%',
                textAlign: 'center',
              }}>
                Graph Info
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <div style={{ display: 'flex', alignItems: 'space-between', margin: 4, }}>
                  <div style={{ fontWeight: 'bold', marginRight: 8 }}>Max Price:</div>
                  The maximum price 1 mDCO can be sold for
                </div>
                <div style={{ display: 'flex', alignItems: 'space-between', margin: 4, }}>
                  <div style={{ fontWeight: 'bold', marginRight: 8 }}>Min Price:</div>
                  <div>The minimum price 1 mDCO can be sold for</div>
                </div>
                <div style={{ display: 'flex', alignItems: 'space-between', margin: 4, }}>
                  <div style={{ fontWeight: 'bold', marginRight: 8 }}>Current Price:</div>
                  <div>The current price to purchase 1 mDCO</div>
                </div>
                <div style={{ display: 'flex', alignItems: 'space-between', margin: 4, }}>
                  <div style={{ fontWeight: 'bold', marginRight: 8 }}>Real Price:</div>
                  <div>The ratio of sold mDCO to Ether, historically</div>
                </div>
              </div>
            </div>
          </div>
        </Popup>
        <div
          style={{
            cursor: 'pointer',
          }}
          onClick={() => this.setState({ showInfoPopup: true }, this.draw)}
          onMouseOver={() => this.setState({ showDetails: true }, this.draw)}
          onMouseOut={() => this.setState({ showDetails: false }, this.draw)}
        >
          <canvas
            style={{
              width: 300,
              height: 300,
              margin: 10,
            }}
            ref={this.canvasRef}
            width={this.state.width}
            height={this.state.height}
          />
        </div>
      </>
    )
  }
}
