import React from 'react'
import { white, gray } from '../Colors'

export default class Input extends React.Component<{
  style?: any
  placeholder?: string
  onChange?: (e: any) => void
  onKeyDown?: (e: any) => void
  value?: any
}>{
  render() {
    return (
      <input
        {...this.props}
        style={{
          padding: 4,
          margin: 4,
          border: `1px solid ${gray}`,
          borderRadius: 0,
          color: gray,
          backgroundColor: white,
          fontSize: 14,
          ...(this.props.style || {}),
        }}
      />
    )
  }
}
