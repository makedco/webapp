import Web3 from 'web3'

declare global {
  interface Window {
    ethereum: any
  }
  namespace NodeJS {
    interface Global {
      web3: Web3
      DECIMAL_PLACES: number
    }
  }
  const DECIMAL_PLACES: number
  let web3: Web3
}
