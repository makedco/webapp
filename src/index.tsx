import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import MakeStore from './stores/make'
import AuthStore from './stores/auth'
import Web3 from 'web3'
import NewSong from './NewSong'

// init MetaMask if it's available
if (typeof web3 !== 'undefined') {
  global.web3 = new Web3(web3.currentProvider)
}

if (window.ethereum) {
  console.log('Requesting MetaMask permission')
  window.ethereum.enable()
}
global.DECIMAL_PLACES = 4

const stores = {
  make: new MakeStore(),
  auth: new AuthStore(),
}
const appDiv = document.getElementById('app')

ReactDOM.render(
  <Provider {...stores}>
    <Router>
      <Route path="/" component={Home} exact />
      <Route path="/whatitbetho" component={NewSong} />
    </Router>
  </Provider>
, appDiv)
