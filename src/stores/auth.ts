import { observable } from 'mobx'
import BN from 'bn.js'
import idx from 'idx'

export default class Auth {
  get activeAddress() {
    return idx(window, (_) => _.ethereum.selectedAddress)
  }

  canMakeTransaction() {
    if (!window.ethereum) return false
    if (!window.ethereum.selectedAddress) return false
    return true
  }

  async send(params: {
    to: string,
    from?: string,
    data: any,
    value?: BN | string
  }) {
    await new Promise((rs, rj) => {
      window.ethereum.sendAsync({
        method: 'eth_sendTransaction',
        params: [{
          to: params.to,
          from: params.from || window.ethereum.selectedAddress,
          data: params.data,
          value: params.value,
        }]
      }, (err: any) => {
        if (err) return rj(err)
        rs()
      })
    })
  }
}
