import { observable } from 'mobx'
import Web3 from 'web3'
import ABI from './makeABI'
import BN from 'bn.js'
import idx from 'idx'
import { Subscription } from 'web3-eth'

export interface Auction {
  startCycle: string|BN
  endCycle: string|BN
  totalTokensSold: string|BN
  totalWeiIngested: string|BN
  creator: string
  name?: string
  url?: string
}

interface ContractSource {
  contractAddress: string
  websocketProviderUrl: string
  websocketOrigin?: string
}

// Still waiting on this
export const homesteadSource: ContractSource = {
  contractAddress: '0x5842dbaef2dd58c94a7974ea9f1fa83b4089ed98',
  // websocketProviderUrl: 'wss://homestead.makedco.io',
  // websocketOrigin: 'makedco',
  websocketProviderUrl: 'wss://mainnet.infura.io/ws/v3/b1a4ca90c9964c329c07874b37e05436',
}

export const rinkebySource: ContractSource = {
  contractAddress: '0x9865420f8ae34e8d3ebce9b90deddb56e28059c0',
  websocketProviderUrl: 'wss://rinkeby.infura.io/ws/v3/b1a4ca90c9964c329c07874b37e05436',
}

export const ropstenSource: ContractSource = {
  contractAddress: '0x1f5ed277cf3e218c388d1d37707687cd389ac8a9',
  websocketProviderUrl: 'wss://ropsten.infura.io/ws/v3/b1a4ca90c9964c329c07874b37e05436',
}

export default class MakeStore {
  @observable creationTimestamp: string|BN = '0'
  @observable totalSupply: string|BN = '0'
  @observable totalWeiIngested: string|BN = '0'
  @observable cycleLengthSeconds: string|BN = '0'
  @observable auctionCount: number = 0
  @observable auctions: Auction[] = []
  @observable weiPrice: string|BN = '0'
  @observable tokensAvailablePerAuction: { [key: number]: number } = {}
  @observable currentCycle: string|BN = '0'
  @observable balances: { [key: string]: number } = {}
  @observable auctionByAddress: { [key: string]: number } = {}

  get averageSellPrice() {
    return +this.totalWeiIngested.toString() / +this.totalSupply.toString()
  }

  // Does a client side calculation using known auctions
  get totalTokensAvailable() {
    let tokens = 0
    for (let x = 1; x < this.auctions.length; x++) {
      tokens += +this.tokensAvailablePerAuction[x]
    }
    return tokens
  }

  contractAddress: string
  public contract: any
  public web3: Web3

  private activeSubscriptions: Subscription<any>[] = []

  @observable
  networkId: number = 0

  constructor() {
    (async () => {
      const firstSubdomain = window.location.hostname.split('.')[0]
      if (firstSubdomain === 'localhost' || firstSubdomain == 'rinkeby') {
        await this.setContractSource(rinkebySource)
      } else if (firstSubdomain === 'ropsten') {
        await this.setContractSource(ropstenSource)
      } else {
        await this.setContractSource(homesteadSource)
      }
      this.registerCycleUpdateHandler()
    })()
      .then(() => console.log('Finished first load'))
      .catch(console.log)
  }

  availableTokens() {
    let tokens = 0
    for (const [key, _tokens] of Object.entries(this.tokensAvailablePerAuction)) {
      if (+key === 0) continue
      tokens += +_tokens || 0
    }
    return tokens
  }

  async loadBalance(address: string) {
    const balance = +await this.contract.methods.balanceOf(address).call()
    this.balances[address] = balance
  }

  // shameless lol
  etherscanPrefix() {
    if (this.networkId === 4) return 'rinkeby.'
    if (this.networkId === 3) return 'ropsten.'
    return ''
  }

  async cycleReload() {
    this.auctionCount = 0
    this.auctions = []
    this.weiPrice = '0'
    this.tokensAvailablePerAuction = {}
    await Promise.all([
      this.loadAuctionCount(),
      this.loadCurrentWeiPrice(),
      this.loadCurrentCycle(),
    ])
    this.registerCycleUpdateHandler()
  }

  /**
   * Bruh, clean this shit up
   **/
  async setContractSource(contractSource: ContractSource) {
    this.activeSubscriptions = this.activeSubscriptions.filter((sub) => {
      sub.unsubscribe()
      return false
    })
    const activeAddress = idx(window, (_) => _.ethereum.selectedAddress)
    this.contractAddress = contractSource.contractAddress
    const provider = new Web3.providers.WebsocketProvider(contractSource.websocketProviderUrl, {
      headers: {
        Origin: contractSource.websocketOrigin,
      },
    })
    this.web3 = new Web3(provider)
    this.contract = new this.web3.eth.Contract(ABI as any, this.contractAddress)
    this.creationTimestamp = '0'
    this.totalSupply = '0'
    this.totalWeiIngested = '0'
    this.cycleLengthSeconds = '0'
    this.auctionCount = 0
    this.auctions = []
    this.weiPrice = '0'
    this.tokensAvailablePerAuction = {}
    this.currentCycle = '0'
    this.balances = {}
    this.networkId = +await this.web3.eth.net.getId()
    this.activeSubscriptions.push(this.contract.events.Purchase({}, async (err: any, event: any) => {
      try {
        if (err) throw err
        await Promise.all([
          this.loadTotalSupply(),
          this.loadTotalWeiIngested(),
          this.loadAuctionIndex(event.returnValues.auctionIndex),
        ])
      } catch (err) {
        console.log('Error loading after purchase event', err)
      }
    }))
    this.activeSubscriptions.push(this.contract.events.AuctionCreated({}, async (err: any, event: any) => {
      try {
        await this.loadAuctionCount()
        await this.loadAuctionIndex(+this.auctionCount - 1)
        setTimeout(async () => {
          await this.loadAuctionCount()
          await this.loadAuctionIndex(+this.auctionCount - 1)
        }, 10000)
      } catch (err) {
        console.log('Error loading after auction created')
      }
    }))
    this.activeSubscriptions.push(
      this.web3.eth.subscribe('newBlockHeaders')
        .on('data', async (block: any, _err: any) => {
          try {
            if (_err) throw _err
            Promise.all([
              this.loadCurrentWeiPrice(),
              activeAddress ? this.loadBalance(activeAddress) : Promise.resolve(),
            ])
          } catch (err) {
            console.log('Error receiving new block', err)
          }
        })
    )
    await Promise.all([
      this.loadCreationTimestamp(),
      this.loadTotalSupply(),
      this.loadTotalWeiIngested(),
      this.loadCycleLengthSeconds(),
      this.loadAuctionCount(),
      this.loadCurrentWeiPrice(),
      this.loadCurrentCycle(),
    ])
    if (activeAddress) {
      await Promise.all([
        this.loadBalance(activeAddress),
        this.loadAuctionForAddress(activeAddress),
      ])
    }
    this.registerCycleUpdateHandler()
  }

  activeTimer: any
  registerCycleUpdateHandler() {
    const now = +new Date() / 1000
    // Calculates without using currentCycle, but is overly long
    const secondsTillNextCycle = +this.cycleLengthSeconds - ((now - +this.creationTimestamp) % +this.cycleLengthSeconds)
    clearTimeout(this.activeTimer)
    this.activeTimer = setTimeout(async () => {
      await this.cycleReload()
    }, secondsTillNextCycle * 1000)
  }

  async loadAuctionForAddress(address: string) {
    this.auctionByAddress[address] = +await this.contract.methods.auctionByAddress(address).call()
  }

  cycleSecondsRemaining() {
    const now = +new Date() / 1000
    return (now - +this.creationTimestamp.toString()) % (+this.cycleLengthSeconds.toString())
  }

  realEtherPrice() {
    if (+this.totalSupply !== 0 && +this.totalWeiIngested !== 0) {
      const weiTokenRatio = new BN(this.totalWeiIngested.toString()).div(new BN(this.totalSupply.toString()))
      return parseFloat(this.web3.utils.fromWei(weiTokenRatio)).toFixed(DECIMAL_PLACES + 2)
    }
    return new BN(0)
  }

  async loadCurrentCycle() {
    this.currentCycle = await this.contract.methods.currentCycle().call() || '0'
  }

  async loadAuctionIndex(index: number) {
    if (index >= this.auctionCount) {
      throw new Error('Auction index out of range')
    }
    const [ auction, tokens ] = await Promise.all([
      this.contract.methods.auctions(index).call(),
      this.contract.methods.tokensAvailableForAuction(index).call(),
    ])
    this.auctions[index] = auction
    this.tokensAvailablePerAuction[index] = tokens
  }

  async loadCurrentWeiPrice() {
    this.weiPrice = await this.contract.methods.currentWeiPrice().call() || '0'
  }

  async loadAuctionCount() {
    const auctionCount = +(await this.contract.methods.auctionCount().call()) || '0'
    this.auctions.length = +auctionCount
    this.auctionCount = +auctionCount
  }

  async loadCreationTimestamp() {
    this.creationTimestamp = await this.contract.methods.creationTimestamp().call() || '0'
  }

  async loadTotalSupply() {
    this.totalSupply = await this.contract.methods.totalSupply().call() || '0'
  }

  async loadTotalWeiIngested() {
    this.totalWeiIngested = await this.contract.methods.totalWeiIngested().call() || '0'
  }

  async loadCycleLengthSeconds() {
    this.cycleLengthSeconds = await this.contract.methods.cycleLengthSeconds().call() || '0'
  }
}
